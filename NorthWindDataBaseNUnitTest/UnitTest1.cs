using FluentAssertions;
using NorthWindDataBaseNUnitTest.Models;
using NUnit.Framework;
using System.Linq;
using System;
using System.Collections.Generic;

namespace NorthWindDataBaseNUnitTest
{
    public class Tests
    {
        [Test]
        public void ObjectHasBeenCreatedInOrdersTable()
        {

            using (NorthwindContext db = new NorthwindContext())
            {
                //test data
                Order testOrder = new Order { ShipName = "TestOrderName", ShipCity = "TestOrderCity" };
                db.Add(testOrder);
                db.SaveChanges();

                //check
                Order tesOderCreated = db.Orders.FirstOrDefault(o => o.ShipName == testOrder.ShipName);
                Console.OpenStandardOutput();
                Console.WriteLine(tesOderCreated.ShipName);

                //delete
                db.Remove(tesOderCreated);
                db.SaveChanges();
            }
        }

        [Test]
        public void FoundDublicateObjectInEmployeeTable()
        {
            using (NorthwindContext db = new NorthwindContext())
            {
                bool result = true;
                List<Employee> allEmployees = (from employee in db.Employees select employee).ToList();

                for (int i = 0; i < allEmployees.Count; i++)
                {
                    var compareBool = allEmployees.Select(e => e.FirstName.Contains(allEmployees[i].FirstName));

                    if (compareBool.Count(x => x == true) > 1)
                    {
                        result = false;
                        break;
                    }
                }

                result.Should().BeTrue("The employee table contain the same objects");
            }
        }

        [Test]
        public void SalesTotalsbyAmountContainsObjectOnlyBetween19970101And19971231() 
        {
            using (NorthwindContext db = new NorthwindContext()) 
            {
                bool result = true;
                DateTime salesDateTimMin = new DateTime(1997, 1, 1, 0, 0, 0);
                DateTime salesDateTimMax = new DateTime(1997, 12, 31, 0, 0, 0);
                List<SalesTotalsByAmount> allSales = (from sales in db.SalesTotalsByAmounts select sales).ToList();

                foreach (SalesTotalsByAmount total in allSales) 
                {
                    if (total.ShippedDate > salesDateTimMax || total.ShippedDate < salesDateTimMin)
                    {
                        result = false;
                        break;
                    }
                }

                result.Should().BeTrue();
            }
        }

        [Test]
        public void SalesByCategoryContainsUnitPriceMoreThenProductsAvgUnitPrice() 
        {
            using (NorthwindContext db = new NorthwindContext()) 
            {
                bool result = true;
                List<Product> allProducts = (from products in db.Products select products).ToList();
                decimal? productAVG = (from products in db.Products select products.UnitPrice).Sum() / allProducts.Count();

                List<decimal?> salesByCategory = (from productsAboveAVG in db.ProductsAboveAveragePrices select productsAboveAVG.UnitPrice).ToList();
                salesByCategory.Sort();

                productAVG.Should().BeLessThan((decimal)salesByCategory[0]);
            }
        }
    }
}